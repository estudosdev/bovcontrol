const { defineFeature, loadFeature } = require('jest-cucumber');
const request = require('supertest');

const container = require('src/container');
const animalFactory = require('tst/support/factory/AnimalFactory');

const httpServer = container.resolve('httpServer');
const feature = loadFeature('tst/support/features/updateAnimal.feature');

defineFeature(feature, (test) => {
  let mockData;
  let newAnimal;
  let response;
  let animalWithNewData;

  test('Success', ({ given, when, then }) => {
    given('A valid payload', async () => {
      mockData = {
        type: 'Teste',
        name: 'Mock name',
        weight: '150',
        age: '8',
      };

      newAnimal = await animalFactory(mockData);
      animalWithNewData = {
        ...newAnimal,
        type: 'Mamifero',
        name: 'Animal alterado',
        weight: '250',
      };
    });

    when('I request the update of a animal', async () => {
      response = await request(httpServer.server)
        .put(`/v1/${newAnimal.id}`)
        .set('Accept', 'application/json')
        .send(animalWithNewData);
    });

    then('I am successful in updated the user', () => {
      const { body, status } = response;

      expect(status).toBe(200);
      expect(body.type).toEqual(animalWithNewData.type);
      expect(body.name).toEqual(animalWithNewData.name);
      expect(body.weight).toEqual(animalWithNewData.weight);
    });
  });

  test('Validation Error', ({ given, when, then }) => {
    given('A invalid payload', async () => {
      mockData = {
        type: 'Teste',
        name: 'Mock name',
        weight: '150',
        age: '8',
      };

      newAnimal = await animalFactory(mockData);
      animalWithNewData = {
        id: newAnimal.id,
        type: 'Mamifero',
        name: 'Animal alterado',
        weight: '250',
      };
    });

    when('I request the update of a animal', async () => {
      response = await request(httpServer.server)
        .put(`/v1/${newAnimal.id}`)
        .set('Accept', 'application/json')
        .send(animalWithNewData);
    });

    then('A validation exception must be returned', () => {
      const { body, status } = response;

      expect(status).toBe(400);
      expect(body.name).toBe('VALIDATION_ERROR');
      expect(body.details.age).toBe('age is required.');
    });
  });

  test('Not Found Error', ({ given, when, then }) => {
    given('A invalid payload', async () => {
      animalWithNewData = {
        id: 'as5f1as5f1as5f',
        type: 'Mamifero',
        name: 'Animal alterado',
        weight: '250',
        age: '8',
      };
    });

    when('I request to update an animal with an id that does not exist', async () => {
      response = await request(httpServer.server)
        .put(`/v1/${animalWithNewData.id}`)
        .set('Accept', 'application/json')
        .send(animalWithNewData);
    });

    then('A not found exception must be returned', () => {
      const { body, status } = response;
      expect(status).toBe(404);
      expect(body.name).toBe('NOTFOUND_ERROR');
      expect(body.details).toBe('Animal not exists');
    });
  });
});

