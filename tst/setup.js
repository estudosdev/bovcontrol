const container = require('src/container');
const cleanDatabase = require('./support/cleanDatabase');

const database = container.resolve('database');

beforeAll(() => {
  database.start(false);
});

beforeEach(done => cleanDatabase(database, done));

afterAll((done) => {
  database.disconnect();
  done();
});
