Feature: Update Animal

Scenario: Success
    Given A valid payload
    When I request the update of a animal
    Then I am successful in updated the user

Scenario: Validation Error
    Given A invalid payload
    When I request the update of a animal
    Then A validation exception must be returned

Scenario: Not Found Error
    Given A invalid payload
    When I request to update an animal with an id that does not exist
    Then A not found exception must be returned
