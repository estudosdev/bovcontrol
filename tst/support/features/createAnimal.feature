Feature: Create Animal

Scenario: Success
    Given A valid payload
    When I request the creation of a new animal
    Then I am successful in creating the user

Scenario: Validation Error
    Given A invalid payload
    When I request the creation of a new animal
    Then A validation exception must be returned
