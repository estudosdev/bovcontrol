Feature: Get Animal

Scenario: Request by type of animal
    Given The type
    When I request that you return the animal data to me
    Then Data is returned to me based on my condition

    Given The type
    When There is no data based on my query
    Then Data is returned to me based on my condition

Scenario: Request By id
    Given The ID
    When I request that you return the animal data to me
    Then Data is returned to me based on my condition

    Given The ID
    When There is no data based on my query
    Then Data is returned to me based on my condition



