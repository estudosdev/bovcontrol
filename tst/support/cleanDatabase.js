module.exports = (database, done) => {
  /* eslint-disable-next-line */
  for (const i in database.db.collections) {
    database.db.collections[i].deleteMany(() => { });
  }
  return done();
};
