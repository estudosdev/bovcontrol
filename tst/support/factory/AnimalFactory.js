const animalModel = require('src/infra/model/animal');

module.exports = async (data) => {
  const {
    _id,
    type,
    name,
    weight,
    age,
  } = await animalModel.create(data);

  return {
    id: _id,
    type,
    name,
    weight,
    age,
  };
};
