const { defineFeature, loadFeature } = require('jest-cucumber');

const GetAnimal = require('src/operations/animal/GetAnimal');

const feature = loadFeature('tst/support/features/getAnimal.feature');


defineFeature(feature, (test) => {
  let mockConditions;
  let response;
  let mockAnimalRepository;
  let getAnimal;
  let animals = [];

  test('Request by type of animal', ({ given, when, then }) => {
    given('The type', () => {
      mockConditions = {
        type: 'Mamiferos',
      };

      animals = [
        {
          id: 'asfasfasfsaasfas15021',
          type: 'Mamiferos',
          name: 'Mock name',
          weight: '150',
          age: '8',
        },
        {
          id: 'asfasfasf1120as15021',
          type: 'Mamiferos',
          name: 'Mock name 2',
          weight: '150',
          age: '8',
        },
        {
          id: 'asfasfasfsaaqweqwe',
          type: 'Mamiferos',
          name: 'Mock name 3',
          weight: '150',
          age: '8',
        },
      ];

      mockAnimalRepository = {
        // eslint-disable-next-line max-len
        find: jest.fn(() => Promise.resolve(animals.filter(animal => animal.type === mockConditions.type))),
      };
    });

    when('I request that you return the animal data to me', () => {
      getAnimal = new GetAnimal({ animalRepository: mockAnimalRepository });
    });

    then('Data is returned to me based on my condition', async () => {
      response = await getAnimal.execute(undefined, mockConditions);
      expect(response.length).toBe(animals.length);
      expect(mockAnimalRepository.find).toHaveBeenCalled();
    });

    given('The type', () => {
      mockConditions = {
        type: 'Mamiferos',
      };

      animals = [
        {
          id: 'asfasfasfsaasfas15021',
          type: 'Teste',
          name: 'Mock name',
          weight: '150',
          age: '8',
        },
        {
          id: 'asfasfasf1120as15021',
          type: 'Teste 2 ',
          name: 'Mock name 2',
          weight: '150',
          age: '8',
        },
        {
          id: 'asfasfasfsaaqweqwe',
          type: 'Teste 3',
          name: 'Mock name 3',
          weight: '150',
          age: '8',
        },
      ];

      mockAnimalRepository = {
        // eslint-disable-next-line max-len
        find: jest.fn(() => Promise.resolve(animals.filter(animal => animal.type === mockConditions.type))),
      };
    });

    when('There is no data based on my query', () => {
      getAnimal = new GetAnimal({ animalRepository: mockAnimalRepository });
    });

    then('Data is returned to me based on my condition', async () => {
      response = await getAnimal.execute(undefined, mockConditions);
      expect(response.length).toBe(0);
    });
  });

  test('Request By id', ({ given, when, then }) => {
    given('The ID', () => {
      mockConditions = {
        id: 'asfasfasfsaasfas15021',
      };

      animals = [
        {
          id: 'asfasfasfsaasfas15021',
          type: 'Teste',
          name: 'Mock name',
          weight: '150',
          age: '8',
        },
        {
          id: 'asfasfasf1120as15021',
          type: 'Teste 2 ',
          name: 'Mock name 2',
          weight: '150',
          age: '8',
        },
        {
          id: 'asfasfasfsaaqweqwe',
          type: 'Teste 3',
          name: 'Mock name 3',
          weight: '150',
          age: '8',
        },
      ];

      mockAnimalRepository = {
        // eslint-disable-next-line max-len
        find: jest.fn(() => Promise.resolve(animals.find(animal => animal.id === mockConditions.id))),
      };
    });

    when('I request that you return the animal data to me', () => {
      getAnimal = new GetAnimal({ animalRepository: mockAnimalRepository });
    });

    then('Data is returned to me based on my condition', async () => {
      response = await getAnimal.execute(mockConditions.id, {});
      expect(response.id).toBe(mockConditions.id);
      expect(response.type).toBe(animals[0].type);
      expect(response.name).toBe(animals[0].name);
      expect(response.weight).toBe(animals[0].weight);
      expect(response.age).toBe(animals[0].age);
      expect(mockAnimalRepository.find).toHaveBeenCalled();
    });

    given('The ID', () => {
      mockConditions = {
        id: 'asfasfasfsaasfas15020',
      };

      animals = [
        {
          id: 'asfasfasfsaasfas15021',
          type: 'Teste',
          name: 'Mock name',
          weight: '150',
          age: '8',
        },
        {
          id: 'asfasfasf1120as15021',
          type: 'Teste 2 ',
          name: 'Mock name 2',
          weight: '150',
          age: '8',
        },
        {
          id: 'asfasfasfsaaqweqwe',
          type: 'Teste 3',
          name: 'Mock name 3',
          weight: '150',
          age: '8',
        },
      ];

      mockAnimalRepository = {
        // eslint-disable-next-line max-len
        find: jest.fn(() => Promise.resolve(animals.find(animal => animal.id === mockConditions.id))),
      };
    });

    when('There is no data based on my query', () => {
      getAnimal = new GetAnimal({ animalRepository: mockAnimalRepository });
    });

    then('Data is returned to me based on my condition', async () => {
      response = await getAnimal.execute(mockConditions.id, {});
      expect(response).toBeUndefined();
    });
  });
});

