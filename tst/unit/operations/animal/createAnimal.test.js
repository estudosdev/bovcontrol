const { defineFeature, loadFeature } = require('jest-cucumber');

const CreateAnimal = require('src/operations/animal/CreateAnimal');

const {
  Validation,
} = require('src/core/Expections');

const feature = loadFeature('tst/support/features/createAnimal.feature');

defineFeature(feature, (test) => {
  let mockData;
  let response;
  let mockAnimalRepository;
  let createAnimal;

  test('Success', ({ given, when, then }) => {
    given('A valid payload', () => {
      mockData = {
        type: 'Teste',
        name: 'Mock name',
        weight: '150',
        age: '8',
      };

      mockAnimalRepository = {
        insert: jest.fn(animal => ({ id: 'aabafeasfsa1051as', ...animal })),
      };
    });

    when('I request the creation of a new animal', () => {
      createAnimal = new CreateAnimal({ animalRepository: mockAnimalRepository });
    });

    then('I am successful in creating the user', async () => {
      response = await createAnimal.execute(mockData);
      expect(response.id).toBe('aabafeasfsa1051as');
      expect(response.type).toBe(mockData.type);
      expect(response.name).toBe(mockData.name);
      expect(response.weight).toBe(mockData.weight);
      expect(response.age).toBe(mockData.age);
      expect(mockAnimalRepository.insert).toHaveBeenCalled();
    });
  });

  test('Validation Error', ({ given, when, then }) => {
    given('A invalid payload', () => {
      mockData = {
        type: 'Teste',
        name: 'Mock name',
        weight: '150',
      };

      mockAnimalRepository = {
        insert: jest.fn(animal => ({ id: 'aabafeasfsa1051as', ...animal })),
      };
    });

    when('I request the creation of a new animal', async () => {
      createAnimal = new CreateAnimal({ animalRepository: mockAnimalRepository });
    });

    then('A validation exception must be returned', () => {
      expect(() => createAnimal.execute(mockData)).toThrow(Validation);
    });
  });
});

