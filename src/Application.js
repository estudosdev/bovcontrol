class Application {
  constructor({ httpServer, database }) {
    this.httpServer = httpServer;
    this.database = database;
  }

  start() {
    this.database.start();
    this.httpServer.start();
  }
}

module.exports = Application;
