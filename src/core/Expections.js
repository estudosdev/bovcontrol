class Validation extends Error {
  constructor(message) {
    super(message);
    this.name = 'VALIDATION_ERROR';
    this.details = message;
  }
}

class NotFound extends Error {
  constructor(message) {
    super(message);
    this.name = 'NOTFOUND_ERROR';
    this.details = message;
  }
}

module.exports = {
  Validation,
  NotFound,
};
