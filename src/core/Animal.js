class Animal {
  constructor(data) {
    this.type = data.type;
    this.name = data.name;
    this.weight = data.weight;
    this.age = data.age;
  }

  checkIntegrity() {}
}

module.exports = Animal;
