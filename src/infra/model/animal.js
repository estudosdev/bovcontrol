const mongoose = require('mongoose');

const AnimalSchema = mongoose.Schema({
  type: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  weight: {
    type: String,
    required: true,
  },
  age: {
    type: String,
    required: true,
  },
});

const Animal = mongoose.model('Animal', AnimalSchema);

module.exports = Animal;
