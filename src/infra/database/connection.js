const mongoose = require('mongoose');
const {
  DatabaseConnection,
} = require('src/infra/expections');

class Database {
  constructor({ databaseConfig }) {
    this.uri = databaseConfig.getURI();
    this.db = mongoose;
  }

  start(useUnifiedTopology) {
    this.createConnection(useUnifiedTopology);
    this.authentication();
    this.defineDBGlobal();
  }

  createConnection(useUnifiedTopology = true) {
    this.db.connect(this.uri, { useNewUrlParser: true, useUnifiedTopology });
    this.db.set('useFindAndModify', false);
    this.connection = this.db.connection;
  }

  async authentication() {
    this.connection.on('error', (error) => {
      throw new DatabaseConnection(error);
    });

    this.connection.once('open', () => {});
  }

  defineDBGlobal() {
    this.db.Promise = global.Promise;
  }

  disconnect() {
    this.db.disconnect();
  }
}

module.exports = Database;
