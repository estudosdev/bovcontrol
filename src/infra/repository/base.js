class BaseRepository {
  constructor(model) {
    this.model = model;
  }

  insert(data) {
    return this.model.create(data);
  }

  find() {
    return this.model.find({});
  }

  update(id, data) {
    return this.model.findOneAndUpdate({ _id: id }, data, { new: true });
  }
}

module.exports = BaseRepository;
