const BaseRepository = require('./base');

class AnimalRepository extends BaseRepository {
  constructor({ animalModel }) {
    super(animalModel);
    this.model = animalModel;
  }

  find(conditions) {
    const { id, type } = conditions;

    if (id) {
      return this.model.findById(id).then(res => res).catch(() => null);
    } else if (type) {
      return this.model.find({ type });
    }

    return super.find();
  }
}

module.exports = AnimalRepository;
