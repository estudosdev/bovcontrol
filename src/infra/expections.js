class DatabaseConnection extends Error {
  constructor(message) {
    super(message);
    this.name = 'DATABASE_CONNECTION_ERROR';
  }
}

module.exports = {
  DatabaseConnection,
};
