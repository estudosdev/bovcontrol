const express = require('express');
const bodyParser = require('body-parser');

class HttpServer {
  constructor({ httpServerConfig, animalControllerFactory }) {
    this.config = httpServerConfig;
    this.animalControllerFactory = animalControllerFactory;
    this.server = express();
    this.setup();
  }

  setup() {
    this.server.use(bodyParser.urlencoded({ extended: true }));
    this.server.use(bodyParser.json());
    this.server.post('/v1', (req, res) => this.animalControllerFactory.create('CREATE', req, res));
    this.server.put('/v1/:id', (req, res) => this.animalControllerFactory.create('UPDATE', req, res));
    this.server.get('/v1/:id', (req, res) => this.animalControllerFactory.create('GET', req, res));
    this.server.get('/v1/', (req, res) => this.animalControllerFactory.create('GET', req, res));
  }

  start() {
    const { PORT } = this.config;
    this.server.listen(PORT, () => console.log('App is running'));
  }
}

module.exports = HttpServer;
