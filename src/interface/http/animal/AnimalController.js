class AnimalController {
  constructor({ createAnimal, getAnimal, updateAnimal }) {
    this.createAnimal = createAnimal;
    this.getAnimal = getAnimal;
    this.updateAnimal = updateAnimal;
  }

  create(req) {
    return this.createAnimal.execute(req.body);
  }

  get(req) {
    const { id } = req.params;

    return this.getAnimal.execute(id, req.query);
  }

  update(req) {
    const { id } = req.params;

    return this.updateAnimal.execute(id, req.body);
  }
}

module.exports = AnimalController;
