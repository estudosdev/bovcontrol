const ProxyController = require('../ProxyController');

class Factory {
  constructor({ createAnimal, animalController }) {
    this.createAnimal = createAnimal;
    this.animalController = animalController;
  }

  create(method, req, res) {
    if (method === 'CREATE') {
      const proxyHandle = new Proxy(
        this.animalController.create.bind(this.animalController),
        new ProxyController(),
      );

      return proxyHandle(req, res);
    } else if (method === 'GET') {
      const proxyHandle = new Proxy(
        this.animalController.get.bind(this.animalController),
        new ProxyController(),
      );

      return proxyHandle(req, res);
    } else if (method === 'UPDATE') {
      const proxyHandle = new Proxy(
        this.animalController.update.bind(this.animalController),
        new ProxyController(),
      );

      return proxyHandle(req, res);
    }

    return {};
  }
}

module.exports = Factory;
