const httpStatus = require('http-status');
const {
  Validation,
  NotFound,
} = require('src/core/Expections');

class ProxyController {
  async apply(target, _thisArg, argumentsList) {
    const [req, res] = argumentsList;

    try {
      const result = await target(req);
      res.status(httpStatus.OK).json(result);
    } catch (error) {
      this.handleErrors(error, res);
    }
  }

  handleErrors(error, res) {
    if (error instanceof Validation) {
      res.status(httpStatus.BAD_REQUEST).json(error);
    } else if (error instanceof NotFound) {
      res.status(httpStatus.NOT_FOUND).json(error);
    }
  }
}

module.exports = ProxyController;
