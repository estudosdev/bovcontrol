const Schema = require('validate');
const {
  Validation,
} = require('src/core/Expections');

const animal = new Schema({
  type: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  weight: {
    type: String,
    required: true,
  },
  age: {
    type: String,
    required: true,
  },
});

const mapperError = (errors) => {
  const result = {};
  errors.map(({ path, message }) => {
    result[path] = message;
    return [path, message];
  });

  return result;
};

module.exports = (data) => {
  const errors = animal.validate(data);
  if (errors.length) {
    throw new Validation(mapperError(errors));
  }
};
