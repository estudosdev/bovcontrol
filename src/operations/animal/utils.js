const {
  NotFound,
} = require('src/core/Expections');

module.exports = {
  validateAnimalExist(animal) {
    if (!animal) {
      throw new NotFound('Animal not exists');
    }
  },
};
