const ApplicationService = require('../Operation');
const Animal = require('src/core/Animal');
const schemaValidation = require('./schema');

class CreateAnimal extends ApplicationService {
  constructor({ animalRepository }) {
    super();
    this.animalRepository = animalRepository;
  }

  execute(data) {
    schemaValidation(data);

    const animal = new Animal(data);
    animal.checkIntegrity();

    return this.animalRepository.insert(animal);
  }
}

module.exports = CreateAnimal;
