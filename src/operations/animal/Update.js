const ApplicationService = require('../Operation');
const Animal = require('src/core/Animal');
const schemaValidation = require('./schema');
const {
  validateAnimalExist,
} = require('./utils');


class Update extends ApplicationService {
  constructor({ animalRepository }) {
    super();
    this.animalRepository = animalRepository;
  }

  async execute(id, data) {
    schemaValidation(data);
    const animalModel = await this.animalRepository.find({ id });
    validateAnimalExist(animalModel, { id });

    const animal = new Animal(data);
    animal.checkIntegrity();

    return this.animalRepository.update(id, animal);
  }
}

module.exports = Update;
