const ApplicationService = require('../Operation');

class GetAnimal extends ApplicationService {
  constructor({ animalRepository }) {
    super();
    this.animalRepository = animalRepository;
  }

  execute(id, { type }) {
    const conditions = {
      id,
      type,
    };

    return this.animalRepository.find(conditions);
  }
}

module.exports = GetAnimal;
