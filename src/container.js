const {
  createContainer, asClass, asValue,
} = require('awilix');

const applicationConfig = require('src-res/config');
const Application = require('./Application');
const HttpServer = require('src/interface/http/Server');
const Database = require('src/infra/database/connection');

const AnimalControllerFactory = require('src/interface/http/animal/AnimalControllerFactory');
const AnimalController = require('src/interface/http/animal/AnimalController');

const AnimalModel = require('src/infra/model/animal');

const AnimalRepository = require('src/infra/repository/animal');

const CreateAnimal = require('src/operations/animal/CreateAnimal');
const GetAnimal = require('src/operations/animal/GetAnimal');
const UpdateAnimal = require('src/operations/animal/Update');

const container = createContainer();

container.register({
  httpServerConfig: asValue(applicationConfig.httpServer),
  databaseConfig: asValue(applicationConfig.database),

  httpServer: asClass(HttpServer).singleton(),
  database: asClass(Database).singleton(),
  app: asClass(Application).singleton(),

  animalModel: asValue(AnimalModel),

  animalRepository: asClass(AnimalRepository).singleton(),

  createAnimal: asClass(CreateAnimal).singleton(),
  updateAnimal: asClass(UpdateAnimal).singleton(),
  getAnimal: asClass(GetAnimal).singleton(),

  animalController: asClass(AnimalController),
  animalControllerFactory: asClass(AnimalControllerFactory).singleton(),
});

module.exports = container;
