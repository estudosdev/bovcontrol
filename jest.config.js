module.exports = {
  setupFilesAfterEnv: [
    './tst/setup.js',
  ],
  verbose: true,
  coverageReporters: ['text-summary', 'html', 'lcov'],
  testEnvironment: 'node',
  collectCoverage: true,
  collectCoverageFrom: [],
  testRegex: '(/__tst__/.*|\\.(test|tst))\\.(js)$',
  moduleFileExtensions: [
    'js',
    'json',
  ],
};
