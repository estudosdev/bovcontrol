**These instructions will get you a copy of the project up and running on your local machine for development and testing purposes**

- [Pre-requisites]
  - [Docker]
  - [Docke-Compose]
- [Architecture]
  - [Clean Code]
  - [Clean Architecture]
- [Libraries]
  - [ExpressJS - https://expressjs.com/pt-br]
  - [Validate - https://github.com/eivindfjeldstad/validate]
  - [Mongoose - https://mongoosejs.com]
  - [JestJS - https://jestjs.io]

## Docker

For development we're using docker with node 13.

## Developing

The port of this appplication is predefined as `3009`

## Getting Started

### Installing
```
    docker-compose run app ash
    npm install
```

Exit the container by typing:

```
    exit
```

## Running

### Start the App

```
    docker-compose up app
```

### Run Tests

```
    docker-compose run tst
```

## Routes

** CREATE ANIMAL **

*POST* ```/v1```
*Example*
```
{
  "type": "22",
  "name": "safsafsaf",
  "weight": "150 Kg",
  "age": "5"
}
```

** UPDATE ANIMAL **

*PUT* ```/v1/:id```
*Example*
```
{
  "type": "22",
  "name": "safsafsaf",
  "weight": "150 Kg",
  "age": "5"
}
```

** GET ANIMAL BY ID **

*GET* ```/v1/:id```
*Example*
```
{
  "_id": "5f1d7c558d18ce001d9f531b",
  "type": "Mamifero",
  "name": "Cavalo",
  "weight": "150 Kg",
  "age": "5",
  "__v": 0
}
```

** GET ANIMAL BY TYPE **

*GET* ```/v1?type=Mamifero```
*Example*
```
[
  {
    "_id": "5f1d78c3b941e200ea6a11c6",
    "type": "Mamifero",
    "name": "Animal 1",
    "weight": "250",
    "age": "8",
    "__v": 0
  },
  {
    "_id": "5f1d78f7b0320f0102e47490",
    "type": "Mamifero",
    "name": "Animal 3",
    "weight": "10",
    "age": "5",
    "__v": 0
  },
  {
    "_id": "5f1d798572e9c60178e5ec59",
    "type": "Mamifero",
    "name": "Animal 2",
    "weight": "50",
    "age": "9",
    "__v": 0
  },
]
```
