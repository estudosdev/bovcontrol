const httpServer = {
  PORT: process.env.APPLICATION_PORT,
};

const database = {
  host: process.env.HOST_DATABASE,
  user: process.env.USER_DATABASE,
  password: process.env.PASSWORD_DATABASE,
  port: process.env.PORT_DATABASE,
  database: process.env.DATABASE,
  getURI() {
    return `mongodb://${this.host}:${this.port}/${this.database}`;
  },
};

const generalConfig = {
  httpServer,
  database,
};

module.exports = generalConfig;
